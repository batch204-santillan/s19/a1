console.log ("ACTIVITY 19")

// PROBLEM 1

let loginInfo

let userName
let password
let role

function userLogin (userName , password , role)
	{
	console.log
				(
				"USER SUMMARY: " + "\n" + 
				"username: " +  userName + "\n" +
				"password: " + 	password + "\n" +
				"role: " + 		role     + "\n"	 		
				)
	}

function inputUserName (userName)
	{
	if 	(userName === null || userName === "") 
		{
		return alert ("Username info should not be empty")	
		}
		else	
			{
			return alert ("Hi " + userName + "!")	
			}	
	}


function inputPassword (password) 
	{
	if 	(password === null || password === "") 
		{
		return alert ("Password should not be empty")	
		}
		else
			{
			return alert ("Don't worry! Your password is safe with us.")	
			}
	}
	
function inputRole (role)
	{
	if 	(role === null || role === "") 
		{
		return alert ("Role should not be empty")	
		}
			else if (role === "admin") 
				{
				return alert ("Welcome back to the class portal, ADMIN!")
				}
			else if (role === "teacher") 
				{
				return alert ("Welcome back to the class portal, TEACHER!")
				}
			else if (role === "student") 
				{
				return alert ("Welcome back to the class portal, STUDENT!")
				}
			else 
				{
				return alert ("Please pick between ADMIN, TEACHER, or STUDENT only.")
				}	
	}
	

	userName = (prompt ("Please enter your user name:")) ;	
		inputUserName (userName) ; 

	password = prompt ("Please enter your password") ;
		inputPassword (password) ;

	role = prompt ("Role: Are you an Admin, Teacher or Student?") .toLowerCase (); 
		inputRole (role) ;


	loginInfo = userLogin (userName , password , role) ;
	console.log (loginInfo) ;


// PROBLEM 2



function getAverage (num1 , num2 , num3 , num4) 
	{
	console.log ("These are your grades:" + "\n" +
				"Math: "	+ num1 + "\n" +
				"Science: "	+ num2 + "\n" +
				"English: "	+ num3 + "\n" +
				"P.E. : "	+ num4 
				)
	return Math.round((num1 + num2 + num3 + num4) / 4) ;
	}

function showGrade (average)
	{
		if (average <= 74)
				{
				console.log ("Your average score is " + average + "\n" + "The letter equivalent is: F")
				}
					else if (average >= 75 && average <= 79)
						{
						console.log ("Your average score is " + average + "\n" + "The letter equivalent is: D")
						}   
					else if (average >= 80 && average <= 84)
						{
						console.log ("Your average score is " + average + "\n" + "The letter equivalent is: C")
						} 
					else if (average >= 85 && average <= 89)
						{
						console.log ("Your average score is " + average + "\n" + "The letter equivalent is: B")
						} 
					else if (average >= 90 && average <= 95)
						{
						console.log ("Your average score is " + average + "\n" + "The letter equivalent is: A")
						} 
					else if (average > 96)
						{
						console.log ("Your average score is " + average + "\n" + "The letter equivalent is: A+")
						} 
	}

//SCENARIO 1
console.log ("SCENARIO 1: F")
let averageGrade = getAverage (70, 72, 75, 77) ;
let finalGrade = showGrade (averageGrade) ;
console.log ("\n")


// SCENARIO 2
console.log ("SCENARIO 2: D")
averageGrade = getAverage (75, 77, 83, 81) ;
finalGrade = showGrade (averageGrade) ;
console.log ("\n")


// SCENARIO 3
console.log ("SCENARIO 3: C")
averageGrade = getAverage (95, 75, 80, 79) ;
finalGrade = showGrade (averageGrade) ;
console.log ("\n")


// SCENARIO 4
console.log ("SCENARIO 4: B")
averageGrade = getAverage (85, 92, 80, 88) ;
finalGrade = showGrade (averageGrade) ;
console.log ("\n")


// SCENARIO 5
console.log ("SCENARIO 5: A")
averageGrade = getAverage (90, 97, 93, 94) ;
finalGrade = showGrade (averageGrade) ;
console.log ("\n")


// SCENARIO 6
console.log ("SCENARIO 6: A+")
averageGrade = getAverage (96, 97, 99, 95) ;
finalGrade = showGrade (averageGrade) ;
console.log ("\n")

// SCENARIO 7 - using prompt

console.log ("SCENARIO 7: using PROMPT")
averageGrade = getAverage 
	(
	parseInt (prompt ("Enter you Math grade:")) , 
	parseInt (prompt ("Enter you Science grade:")) ,
	parseInt (prompt ("Enter you English grade:")) ,
	parseInt (prompt ("Enter you P.E. grade:")) 
	) ;
finalGrade = showGrade (averageGrade) ;
console.log ("\n")
